﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace basicPinger
{
    public partial class mainForm : Form
    {
        public static Boolean isMainForm;
        public static Result r;
        MiniForm frm2;
        private System.Windows.Forms.Timer timer1;
        Boolean isContinue;
        String hostIP;
        PingReply oldReply;
        DateTime startedAt;

        public mainForm()
        {
            InitializeComponent();
            initFormParameters();
        }

        ///////////////////////////////
        private async void startButton_ClickAsync(object sender, EventArgs e)
        {
            // Make sure the inputs aren't blank
            if (!CheckInputs())
                ShowMessageBox("Please make sure all of the parameters are defined correctly.", MessageBoxIcon.Error, "Bad input");

            // Make sure it's not running already
            if (isContinue)
                return;

            changeFormButton.Show();
            isContinue = true;
            hostIP = ipTextBox.Text;
            int amount = (infiniteCheckBox.Checked) ? int.MaxValue : int.Parse(amountTextBox.Text);
            r = new Result(0, 0, 0, 0);

            startedAt = DateTime.Now;
            logRichTextBox.AppendText("*****Started Date: " + DateTime.Now.ToString("dd-MM-yyyyTHH:mm:ss") + "*****\r\n");
            logRichTextBox.AppendText("Start pinging : " + hostIP + " for the amount of : " + amount + ";");

            if (autoSaveLogCheckBox.Checked)
                InitTimer();

            for (int i = 0; i < amount; i++)
            {
                Ping pingSender = new Ping();
                r = await PingAndProcessAsync(pingSender, hostIP, r);
                if (!isContinue)
                    break;

                if (isMainForm)
                {
                    this.Show();
                    frm2.Hide();
                    UpdateOnScreenResult(r);
                }
                else
                {
                    this.Hide();
                    frm2.Show();
                    UpdateMiniFormResult(r);
                }

            }

            PrintResultToLog(r);

            DateTime stoppedAt = DateTime.Now;
            logRichTextBox.AppendText("*****Stopped Date: " + DateTime.Now.ToString("dd-MM-yyyyTHH:mm:ss") + "*****\r\n");
            logRichTextBox.AppendText("Finished pinging : " + hostIP + " for the amount of : " + r.Total + "\r\n");
            logRichTextBox.AppendText("Been running for: " + 
                    (stoppedAt - startedAt).Days + " Days, " +
                    (stoppedAt - startedAt).Hours + " Hours, " + 
                    (stoppedAt - startedAt).Minutes + " Minutes and " +
                    (stoppedAt - startedAt).Seconds + " Seconds. \r\n");

        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            changeFormButton.Hide();
            isContinue = false;
            if (timer1.Enabled)
                timer1.Stop();
        }

        private void infiniteCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            amountTextBox.Enabled = (!infiniteCheckBox.Checked);
        }

        private void logRichTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void saveLogButton_Click(object sender, EventArgs e)
        {
            SaveLog();
        }

        private void clearLogButton_Click(object sender, EventArgs e)
        {
            if (isContinue)
            {
                ShowMessageBox("You have to stop before clearing the current log.", MessageBoxIcon.Error, "Clearing log error");
                return;
            }

            logRichTextBox.ResetText();
        }

        private void openSavedLogButton_Click(object sender, EventArgs e)
        {
            String filePath = "C://BasicPinger//ping-log.txt";
            if (!File.Exists(filePath))
            {
                ShowMessageBox("There isn't a saved log yet!", MessageBoxIcon.Error, "Error opening saved log");
                return;
            }

            Process myProcess = new Process();
            Process.Start("notepad++.exe", filePath);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            SaveLog();
        }

        /////////////////////////////////

        /// <summary>
        /// function to define parameters so the constructor won't have a lot of code
        /// </summary>
        public void initFormParameters()
        {
            // set frm2
            frm2 = new MiniForm();
            frm2.ParentForm = this;

            isMainForm = true;
            isContinue = false;
            oldReply = null;

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;

            this.autoSaveLogComboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// This function checks if the inserted inputs are correct
        /// </summary>
        /// <returns> true if they are, else false </returns>
        private bool CheckInputs()
        {
            String ip = ipTextBox.Text;
            if (!ValidateIPv4(ip))
                return false;

            if (IsNull(amountTextBox.Text) && !infiniteCheckBox.Checked)
                return false;

            return true;


        }

        /// <summary>
        /// This functions tries to ping the given host
        /// </summary>
        /// <returns> true if succeeded, else false </returns>
        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = null;

            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
                return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }

            return pingable;
        }

        /// <summary>
        /// This function checks if the given input is an ip address
        /// </summary>
        /// <returns> true if it is an ip address, else false </returns>
        public bool ValidateIPv4(string ipString)
        {
            if (String.IsNullOrWhiteSpace(ipString))
            {
                return false;
            }

            string[] splitValues = ipString.Split('.');
            if (splitValues.Length != 4)
            {
                return false;
            }

            byte tempForParsing;

            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }

        /// <summary>
        /// This function checks if the given str is null in this project
        /// </summary>
        /// <returns> true if it is, else false </returns>
        public bool IsNull(String str)
        {
            String[] nullArray = { "", null };
            foreach (string s in nullArray)
                if (str.Equals(s)) return true;

            return false;
        }

        /// <summary>
        /// This function shows a messagebox with the given str
        /// </summary>
        public void ShowMessageBox(String text, MessageBoxIcon icon, String title = "MessageBox")
        {
            MessageBox.Show(text, title, MessageBoxButtons.OK, icon);
        }

        /// <summary>
        /// This function prints to the user the result of the ping operations
        /// </summary>
        public void PrintResultToLog(Result r)
        {
            String asteriskStr = "********************";
            //start
            logRichTextBox.AppendText("\r\n");
            for (int i = 5; i >= 1; i--)
            {
                for (int j = 1; j <= i; j++)
                {
                    logRichTextBox.AppendText(asteriskStr);
                }
                logRichTextBox.AppendText("\r\n");
            }

            //print result
            logRichTextBox.AppendText("\r\n" + "Result:");
            logRichTextBox.AppendText("\r\n" + "Success : " + r.Successes + " .");
            logRichTextBox.AppendText("\r\n" + "Fails : " + r.Fails + " .");
            logRichTextBox.AppendText("\r\n" + "Total : " + r.Total + " .");


            //end
            logRichTextBox.AppendText("\r\n");
            for (int i = 0; i <= 5; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    logRichTextBox.AppendText(asteriskStr);
                }
                logRichTextBox.AppendText("\r\n");
            }

            logRichTextBox.ScrollToCaret();
        }

        /// <summary>
        /// This function updates the result parameters on the screen
        /// </summary>
        public void UpdateOnScreenResult(Result r)
        {
            successAmountTextBox.Text = r.Successes.ToString();
            failAmountTextBox.Text = r.Fails.ToString();
            totalAmountTextBox.Text = r.Total.ToString();
        }

        /// <summary>
        /// function to update the mini form if we're showing the mini form
        /// </summary>
        /// <param name="r">the current result</param>
        public void UpdateMiniFormResult(Result r)
        {
            frm2.SetSuccessAmount(r.Successes.ToString());
            frm2.SetFailAmount(r.Fails.ToString());
            frm2.SetTotalAmount(r.Total.ToString());
        }

        /// <summary>
        /// This function pings the given host and sends the result to @ProcessPingResult to process it
        /// </summary>
        /// <returns> the returned result from @ProcessPingResult </returns>
        private async Task<Result> PingAndProcessAsync(Ping pingSender, string ip, Result r)
        {
            var result = await pingSender.SendPingAsync(ip, 2000);
            return ProcessPingResult(result, r);
        }

        /// <summary>
        /// This function processes the ping results and returns it
        /// </summary>
        /// <returns> returns : the result </returns>
        private Result ProcessPingResult(PingReply result, Result r)
        {
            if (oldReply == null)
                oldReply = result;

            if (oldReply.Status == result.Status)
                r.Count++;
            else
                r.Count = 1;

            if (r.Count > 1)
            {
                var last = logRichTextBox.Text.LastIndexOf(";");
                if (last > 0)
                {
                    logRichTextBox.Text = logRichTextBox.Text.Substring(0, last - 1);
                    var beforelast = logRichTextBox.Text.LastIndexOf(";");
                    logRichTextBox.Text = logRichTextBox.Text.Substring(0, beforelast + 1);
                }
            }
            if (result.Status == IPStatus.Success)
            {
                logRichTextBox.AppendText("\r\n" + "pinged : " + hostIP + " successfully. x" + r.Count + ";");
                r.Successes++;
            }
            else
            {
                logRichTextBox.AppendText("\r\n" + "error pinging : " + hostIP + " . x" + r.Count + ";");
                r.Fails++;
            }

            oldReply = result;

            r.Total++;

            if (r.Count < 2)
                logRichTextBox.ScrollToCaret();

            return r;
        }

        /// <summary>
        /// function to save the current log
        /// </summary>
        private void SaveLog()
        {
            if (isContinue && !autoSaveLogCheckBox.Checked)
            {
                ShowMessageBox("You have to stop before saving the current log.", MessageBoxIcon.Error, "Saving log error");
                return;
            }

            if (autoSaveLogCheckBox.Checked)
                PrintResultToLog(r);

            String path = "C://BasicPinger//";

            Directory.CreateDirectory(path);
            logRichTextBox.SaveFile(path + "ping-log" + ".txt", RichTextBoxStreamType.PlainText);


            if (!autoSaveLogCheckBox.Checked)
                ShowMessageBox("File saved successfully!", MessageBoxIcon.Information, "Operation succeeded");
        }

        /// <summary>
        /// function to init the autoSaveLog timer
        /// </summary>
        public void InitTimer()
        {
            int seconds;
            if (autoSaveLogComboBox.SelectedItem == null)
                seconds = 60000;
            else
                seconds = int.Parse((autoSaveLogComboBox.SelectedItem.ToString()).Substring(0, 1)) * 1000 * 60;

            timer1 = new System.Windows.Forms.Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = seconds; // in miliseconds
            timer1.Start();
        }
        
        //////////////////////////////
        public struct Result
        {
            public int Successes;
            public int Fails;
            public int Total;
            public int Count;
            public Result(int successes, int fails, int total, int count)
            {
                Successes = successes;
                Fails = fails;
                Total = total;
                Count = count;
            }
        }

        private void autoSaveLogCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            autoSaveLogComboBox.Enabled = (autoSaveLogCheckBox.Checked);
        }

        private void changeFormButton_Click(object sender, EventArgs e)
        {
            isMainForm = !isMainForm;
        }
        
    }
}
