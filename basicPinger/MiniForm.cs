﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace basicPinger
{
    public partial class MiniForm : Form
    {
        public String txtval { get; set; }
        public Form ParentForm { get; set; }
        public MiniForm()
        {
            InitializeComponent();
            initFormParameters();
        }

        private void changeFormButton_Click(object sender, EventArgs e)
        {
            this.ParentForm.Show();
            
            mainForm.isMainForm = true;
        }

        private void MiniForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (e.CloseReason == CloseReason.UserClosing)
            //{
            //    dynamic result = MessageBox.Show("Do You Want To Exit?", "Application Name", MessageBoxButtons.YesNo);
            //    if (result == DialogResult.Yes)
            //    {
                    Application.Exit();
                //}

                //if (result == DialogResult.No)
                //{
                //    e.Cancel = true;
                //}
            //}
        }

        private void MiniForm_Load(object sender, EventArgs e)
        {

        }

        public void initFormParameters()
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }

        public void SetSuccessAmount(String amount)
        {
            successAmountTextBox.Text = amount;
        }

        public void SetFailAmount(String amount)
        {
            failAmountTextBox.Text = amount;
        }

        public void SetTotalAmount(String amount)
        {
            totalAmountTextBox.Text = amount;
        }
    }
}
