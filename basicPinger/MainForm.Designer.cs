﻿namespace basicPinger
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.amountLabel = new System.Windows.Forms.Label();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.infiniteCheckBox = new System.Windows.Forms.CheckBox();
            this.logRichTextBox = new System.Windows.Forms.RichTextBox();
            this.logLabel = new System.Windows.Forms.Label();
            this.ipTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.successAmountTextBox = new System.Windows.Forms.TextBox();
            this.successAmountLabel = new System.Windows.Forms.Label();
            this.failAmountTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.totalAmountTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.runStatus = new System.Windows.Forms.Label();
            this.saveLogButton = new System.Windows.Forms.Button();
            this.clearLogButton = new System.Windows.Forms.Button();
            this.openSavedLogButton = new System.Windows.Forms.Button();
            this.autoSaveLogCheckBox = new System.Windows.Forms.CheckBox();
            this.autoSaveLogComboBox = new System.Windows.Forms.ComboBox();
            this.changeFormButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(268, 406);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 3;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_ClickAsync);
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(368, 406);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 4;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // amountLabel
            // 
            this.amountLabel.AutoSize = true;
            this.amountLabel.Location = new System.Drawing.Point(22, 63);
            this.amountLabel.Name = "amountLabel";
            this.amountLabel.Size = new System.Drawing.Size(48, 13);
            this.amountLabel.TabIndex = 2;
            this.amountLabel.Text = "amount: ";
            // 
            // amountTextBox
            // 
            this.amountTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.amountTextBox.Enabled = false;
            this.amountTextBox.Location = new System.Drawing.Point(77, 63);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.Size = new System.Drawing.Size(100, 20);
            this.amountTextBox.TabIndex = 1;
            // 
            // infiniteCheckBox
            // 
            this.infiniteCheckBox.AutoSize = true;
            this.infiniteCheckBox.Checked = true;
            this.infiniteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.infiniteCheckBox.Location = new System.Drawing.Point(25, 96);
            this.infiniteCheckBox.Name = "infiniteCheckBox";
            this.infiniteCheckBox.Size = new System.Drawing.Size(57, 17);
            this.infiniteCheckBox.TabIndex = 2;
            this.infiniteCheckBox.Text = "Infinite";
            this.infiniteCheckBox.UseVisualStyleBackColor = true;
            this.infiniteCheckBox.CheckedChanged += new System.EventHandler(this.infiniteCheckBox_CheckedChanged);
            // 
            // logRichTextBox
            // 
            this.logRichTextBox.Location = new System.Drawing.Point(267, 60);
            this.logRichTextBox.Name = "logRichTextBox";
            this.logRichTextBox.ReadOnly = true;
            this.logRichTextBox.Size = new System.Drawing.Size(506, 327);
            this.logRichTextBox.TabIndex = 4;
            this.logRichTextBox.Text = "";
            this.logRichTextBox.TextChanged += new System.EventHandler(this.logRichTextBox_TextChanged);
            // 
            // logLabel
            // 
            this.logLabel.AutoSize = true;
            this.logLabel.Location = new System.Drawing.Point(521, 33);
            this.logLabel.Name = "logLabel";
            this.logLabel.Size = new System.Drawing.Size(21, 13);
            this.logLabel.TabIndex = 5;
            this.logLabel.Text = "log";
            // 
            // ipTextBox
            // 
            this.ipTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.ipTextBox.Location = new System.Drawing.Point(77, 33);
            this.ipTextBox.Name = "ipTextBox";
            this.ipTextBox.Size = new System.Drawing.Size(100, 20);
            this.ipTextBox.TabIndex = 0;
            this.ipTextBox.Text = "8.8.8.8";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "ip:";
            // 
            // successAmountTextBox
            // 
            this.successAmountTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.successAmountTextBox.Enabled = false;
            this.successAmountTextBox.Location = new System.Drawing.Point(77, 296);
            this.successAmountTextBox.Name = "successAmountTextBox";
            this.successAmountTextBox.Size = new System.Drawing.Size(100, 20);
            this.successAmountTextBox.TabIndex = 8;
            // 
            // successAmountLabel
            // 
            this.successAmountLabel.AutoSize = true;
            this.successAmountLabel.Location = new System.Drawing.Point(22, 296);
            this.successAmountLabel.Name = "successAmountLabel";
            this.successAmountLabel.Size = new System.Drawing.Size(49, 13);
            this.successAmountLabel.TabIndex = 9;
            this.successAmountLabel.Text = "success:";
            // 
            // failAmountTextBox
            // 
            this.failAmountTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.failAmountTextBox.Enabled = false;
            this.failAmountTextBox.Location = new System.Drawing.Point(77, 335);
            this.failAmountTextBox.Name = "failAmountTextBox";
            this.failAmountTextBox.Size = new System.Drawing.Size(100, 20);
            this.failAmountTextBox.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 335);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "fail:";
            // 
            // totalAmountTextBox
            // 
            this.totalAmountTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.totalAmountTextBox.Enabled = false;
            this.totalAmountTextBox.Location = new System.Drawing.Point(77, 367);
            this.totalAmountTextBox.Name = "totalAmountTextBox";
            this.totalAmountTextBox.Size = new System.Drawing.Size(100, 20);
            this.totalAmountTextBox.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 367);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "total:";
            // 
            // runStatus
            // 
            this.runStatus.AutoSize = true;
            this.runStatus.Location = new System.Drawing.Point(74, 258);
            this.runStatus.Name = "runStatus";
            this.runStatus.Size = new System.Drawing.Size(35, 13);
            this.runStatus.TabIndex = 14;
            this.runStatus.Text = "status";
            // 
            // saveLogButton
            // 
            this.saveLogButton.Location = new System.Drawing.Point(472, 406);
            this.saveLogButton.Name = "saveLogButton";
            this.saveLogButton.Size = new System.Drawing.Size(75, 23);
            this.saveLogButton.TabIndex = 15;
            this.saveLogButton.Text = "Save Log";
            this.saveLogButton.UseVisualStyleBackColor = true;
            this.saveLogButton.Click += new System.EventHandler(this.saveLogButton_Click);
            // 
            // clearLogButton
            // 
            this.clearLogButton.Location = new System.Drawing.Point(707, 406);
            this.clearLogButton.Name = "clearLogButton";
            this.clearLogButton.Size = new System.Drawing.Size(75, 23);
            this.clearLogButton.TabIndex = 16;
            this.clearLogButton.Text = "Clear Log";
            this.clearLogButton.UseVisualStyleBackColor = true;
            this.clearLogButton.Click += new System.EventHandler(this.clearLogButton_Click);
            // 
            // openSavedLogButton
            // 
            this.openSavedLogButton.Location = new System.Drawing.Point(574, 406);
            this.openSavedLogButton.Name = "openSavedLogButton";
            this.openSavedLogButton.Size = new System.Drawing.Size(106, 23);
            this.openSavedLogButton.TabIndex = 17;
            this.openSavedLogButton.Text = "Open Saved Log";
            this.openSavedLogButton.UseVisualStyleBackColor = true;
            this.openSavedLogButton.Click += new System.EventHandler(this.openSavedLogButton_Click);
            // 
            // autoSaveLogCheckBox
            // 
            this.autoSaveLogCheckBox.AutoSize = true;
            this.autoSaveLogCheckBox.Checked = true;
            this.autoSaveLogCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoSaveLogCheckBox.Location = new System.Drawing.Point(25, 119);
            this.autoSaveLogCheckBox.Name = "autoSaveLogCheckBox";
            this.autoSaveLogCheckBox.Size = new System.Drawing.Size(123, 17);
            this.autoSaveLogCheckBox.TabIndex = 18;
            this.autoSaveLogCheckBox.Text = "Auto save log every:";
            this.autoSaveLogCheckBox.UseVisualStyleBackColor = true;
            this.autoSaveLogCheckBox.CheckedChanged += new System.EventHandler(this.autoSaveLogCheckBox_CheckedChanged);
            // 
            // autoSaveLogComboBox
            // 
            this.autoSaveLogComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.autoSaveLogComboBox.FormattingEnabled = true;
            this.autoSaveLogComboBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.autoSaveLogComboBox.Items.AddRange(new object[] {
            "1 minute",
            "5 minutes",
            "10 minutes",
            "30 minutes",
            "60 minutes"});
            this.autoSaveLogComboBox.Location = new System.Drawing.Point(154, 117);
            this.autoSaveLogComboBox.Name = "autoSaveLogComboBox";
            this.autoSaveLogComboBox.Size = new System.Drawing.Size(107, 21);
            this.autoSaveLogComboBox.TabIndex = 19;
            // 
            // changeFormButton
            // 
            this.changeFormButton.Location = new System.Drawing.Point(66, 406);
            this.changeFormButton.Name = "changeFormButton";
            this.changeFormButton.Size = new System.Drawing.Size(125, 23);
            this.changeFormButton.TabIndex = 20;
            this.changeFormButton.Text = "Change to Basic Info";
            this.changeFormButton.UseVisualStyleBackColor = true;
            this.changeFormButton.Visible = false;
            this.changeFormButton.Click += new System.EventHandler(this.changeFormButton_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.changeFormButton);
            this.Controls.Add(this.autoSaveLogComboBox);
            this.Controls.Add(this.autoSaveLogCheckBox);
            this.Controls.Add(this.openSavedLogButton);
            this.Controls.Add(this.clearLogButton);
            this.Controls.Add(this.saveLogButton);
            this.Controls.Add(this.runStatus);
            this.Controls.Add(this.totalAmountTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.failAmountTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.successAmountTextBox);
            this.Controls.Add(this.successAmountLabel);
            this.Controls.Add(this.ipTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.logLabel);
            this.Controls.Add(this.logRichTextBox);
            this.Controls.Add(this.infiniteCheckBox);
            this.Controls.Add(this.amountTextBox);
            this.Controls.Add(this.amountLabel);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startButton);
            this.Name = "mainForm";
            this.Text = "Basic Pinger";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Label amountLabel;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.CheckBox infiniteCheckBox;
        private System.Windows.Forms.RichTextBox logRichTextBox;
        private System.Windows.Forms.Label logLabel;
        private System.Windows.Forms.TextBox ipTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox successAmountTextBox;
        private System.Windows.Forms.Label successAmountLabel;
        private System.Windows.Forms.TextBox failAmountTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox totalAmountTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label runStatus;
        private System.Windows.Forms.Button saveLogButton;
        private System.Windows.Forms.Button clearLogButton;
        private System.Windows.Forms.Button openSavedLogButton;
        private System.Windows.Forms.CheckBox autoSaveLogCheckBox;
        private System.Windows.Forms.ComboBox autoSaveLogComboBox;
        private System.Windows.Forms.Button changeFormButton;
    }
}

