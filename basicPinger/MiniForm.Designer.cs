﻿namespace basicPinger
{
    partial class MiniForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.runStatus = new System.Windows.Forms.Label();
            this.totalAmountTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.failAmountTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.successAmountTextBox = new System.Windows.Forms.TextBox();
            this.successAmountLabel = new System.Windows.Forms.Label();
            this.changeFormButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // runStatus
            // 
            this.runStatus.AutoSize = true;
            this.runStatus.Location = new System.Drawing.Point(111, 36);
            this.runStatus.Name = "runStatus";
            this.runStatus.Size = new System.Drawing.Size(35, 13);
            this.runStatus.TabIndex = 21;
            this.runStatus.Text = "status";
            // 
            // totalAmountTextBox
            // 
            this.totalAmountTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.totalAmountTextBox.Enabled = false;
            this.totalAmountTextBox.Location = new System.Drawing.Point(114, 145);
            this.totalAmountTextBox.Name = "totalAmountTextBox";
            this.totalAmountTextBox.Size = new System.Drawing.Size(100, 20);
            this.totalAmountTextBox.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "total:";
            // 
            // failAmountTextBox
            // 
            this.failAmountTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.failAmountTextBox.Enabled = false;
            this.failAmountTextBox.Location = new System.Drawing.Point(114, 113);
            this.failAmountTextBox.Name = "failAmountTextBox";
            this.failAmountTextBox.Size = new System.Drawing.Size(100, 20);
            this.failAmountTextBox.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "fail:";
            // 
            // successAmountTextBox
            // 
            this.successAmountTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.successAmountTextBox.Enabled = false;
            this.successAmountTextBox.Location = new System.Drawing.Point(114, 74);
            this.successAmountTextBox.Name = "successAmountTextBox";
            this.successAmountTextBox.Size = new System.Drawing.Size(100, 20);
            this.successAmountTextBox.TabIndex = 15;
            // 
            // successAmountLabel
            // 
            this.successAmountLabel.AutoSize = true;
            this.successAmountLabel.Location = new System.Drawing.Point(59, 74);
            this.successAmountLabel.Name = "successAmountLabel";
            this.successAmountLabel.Size = new System.Drawing.Size(49, 13);
            this.successAmountLabel.TabIndex = 16;
            this.successAmountLabel.Text = "success:";
            // 
            // changeFormButton
            // 
            this.changeFormButton.Location = new System.Drawing.Point(62, 182);
            this.changeFormButton.Name = "changeFormButton";
            this.changeFormButton.Size = new System.Drawing.Size(125, 23);
            this.changeFormButton.TabIndex = 22;
            this.changeFormButton.Text = "Change to Full Info";
            this.changeFormButton.UseVisualStyleBackColor = true;
            this.changeFormButton.Click += new System.EventHandler(this.changeFormButton_Click);
            // 
            // MiniForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 226);
            this.Controls.Add(this.changeFormButton);
            this.Controls.Add(this.runStatus);
            this.Controls.Add(this.totalAmountTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.failAmountTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.successAmountTextBox);
            this.Controls.Add(this.successAmountLabel);
            this.Name = "MiniForm";
            this.Text = "MiniForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MiniForm_FormClosing);
            this.Load += new System.EventHandler(this.MiniForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label runStatus;
        private System.Windows.Forms.TextBox totalAmountTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox failAmountTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox successAmountTextBox;
        private System.Windows.Forms.Label successAmountLabel;
        private System.Windows.Forms.Button changeFormButton;
    }
}